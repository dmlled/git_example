proc process {i} {
	puts $i
}

for {set i 0} {$i < 42} {incr i} {
	process $i
}

puts "the end!"